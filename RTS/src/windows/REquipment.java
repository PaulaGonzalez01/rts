/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import TDAS.Equipment;
import TDAS.Technician;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author pcgfd
 */
public class REquipment {

    private Label title;
    private TableView table;
    private ObservableList<Equipment> eqs;
    private ObservableList<String> typesol;
    private ObservableList<String> areasol;
    private ObservableList<String> statusol;
    private ArrayList<Equipment> equipments;
    private ArrayList<String> types;
    private ArrayList<String> areas;
    private ArrayList<String> statuses;
    private Label register;
    private Label code;
    private Label type;
    private Label brand;
    private Label area;
    private Label tec;
    private Label email;
    private Label status;
    private TextField txtcode;
    private TextField txtbrand;
    private TextField txttec;
    private TextField txtemail;
    private ComboBox ctype;
    private ComboBox carea;
    private ComboBox cstatus;
    private Button bregis;
    private Button breturn;
    private VBox tablebox;
    private HBox b1;
    private HBox b2;
    private HBox b3;
    private HBox b4;
    private VBox databox;
    private VBox root;
    private Scene scene;
    private Stage stage;

    public REquipment(Stage s) {
        stage = s;
        inicialize();
        setTableView();
        fillCombos();
        giveActions();
        setIds();

    }

    private void inicialize() {
        title = new Label("Últimos equipos");
        table = new TableView();
        equipments = new ArrayList<>();
        types = new ArrayList<>();
        areas = new ArrayList<>();
        statuses = new ArrayList<>();
        eqs = FXCollections.observableArrayList(equipments);
        typesol = FXCollections.observableArrayList(types);
        areasol = FXCollections.observableArrayList(areas);
        statusol = FXCollections.observableArrayList(statuses);
        register = new Label("Registro de datos");
        code = new Label("Código: ");
        type = new Label("Tipo: ");
        brand = new Label("Marca: ");
        area = new Label("Área: ");
        tec = new Label("Técnico(usuario): ");
        email = new Label("Correo: ");
        status = new Label("Estado: ");
        txtcode = new TextField();
        txtbrand = new TextField();
        txtemail = new TextField();
        txttec = new TextField();
        carea = new ComboBox();
        cstatus = new ComboBox();
        ctype = new ComboBox();
        bregis = new Button("Registrar");
        breturn = new Button("Menú principal");
        tablebox = new VBox(title, table);
        b1 = new HBox(code, txtcode, area, carea);
        b2 = new HBox(type, ctype, tec, txttec);
        b3 = new HBox(brand, txtbrand, email, txtemail);
        b4 = new HBox(status, cstatus, bregis, breturn);
        databox = new VBox(register, b1, b2, b3, b4);
        root = new VBox(tablebox, databox);
        scene = new Scene(root, 500, 600);
    }

    private void setIds() {
        table.setId("table");
    }

    private void giveActions() {
        breturn.setOnAction(e -> regresar());
        bregis.setOnAction(e -> registrar());
    }

    private void fillCombos() {
        System.out.println("Inside fillCombosx");
        types.add("MIC");
        types.add("CAM");
        types.add("LUC");
        types.add("EIP");
        types.add("P2M");
        types.add("BTC");
        types.add("TRI");

        areas.add("MNO");
        areas.add("INC");
        areas.add("IDM");
        areas.add("RMC");
        areas.add("SUN");
        areas.add("ELU");
        areas.add("CMC");
        areas.add("P2M");
        areas.add("BAT");
        areas.add("TRI");

        statuses.add("1 Entrada");
        statuses.add("2 Salida");

        typesol.addAll(types);
        ctype.setItems(typesol);

        areasol.addAll(areas);
        carea.setItems(areasol);

        statusol.addAll(statuses);
        cstatus.setItems(statusol);

    }

    public Scene getScene() {
        return scene;
    }

    public ArrayList<Equipment> getEquipments() {
        ArrayList<Equipment> equipments = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader("ccc2.csv"))) {
            int i = 0;
            while (bf.ready()) {
                if (i > 0) {
                    String[] list = bf.readLine().split(";");
                    System.out.println(list[0] + list[1] + list[2] + list[3] + list[4] + list[5] + list[6] + list[7] + list[8] + list[9]);
                    String[] ta = list[14].split("/");
                    System.out.println(ta[0] + ta[1]);
                    Equipment e = new Equipment(list[0], ta[0], list[1], ta[1], new Technician(list[8]), list[13]);
                    equipments.add(e);
                } else {
                    i++;
                }

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(REquipment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(REquipment.class.getName()).log(Level.SEVERE, null, ex);
        }
        return equipments;
    }

    private void setTableView() {
        TableColumn<Equipment, String> codeCol = new TableColumn<>("Código");
        codeCol.setMinWidth(200);
        codeCol.setCellValueFactory(new PropertyValueFactory("code"));

        TableColumn<Equipment, Integer> typeCol = new TableColumn<>("Tipo");
        typeCol.setMinWidth(100);
        typeCol.setCellValueFactory(new PropertyValueFactory("type"));

        TableColumn<Equipment, Integer> brandCol = new TableColumn<>("Marca");
        brandCol.setMinWidth(100);
        brandCol.setCellValueFactory(new PropertyValueFactory("brand"));

        TableColumn<Equipment, String> areaCol = new TableColumn<>("Área");
        areaCol.setMinWidth(400);
        areaCol.setCellValueFactory(new PropertyValueFactory("area"));

        TableColumn<Equipment, Technician> tecCol = new TableColumn<>("Técnico");
        tecCol.setMinWidth(400);
        tecCol.setCellValueFactory(new PropertyValueFactory("tec"));

        TableColumn<Equipment, String> statCol = new TableColumn<>("Estado");
        statCol.setMinWidth(400);
        statCol.setCellValueFactory(new PropertyValueFactory("status"));

        eqs.addAll(getEquipments());
        table.setItems(eqs);
        table.getColumns().addAll(codeCol, typeCol, brandCol, areaCol, tecCol, statCol);

    }

    private void regresar() {
        Main m = new Main(stage);
        stage.setScene(m.getScene());
    }

    private void registrar() {

        try (PrintWriter p = new PrintWriter(new FileWriter("ccc2.csv", true), false)) {
            Date date = Calendar.getInstance().getTime();

            // Display a date in day, month, year format
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String fecha = formatter.format(date);
            System.out.println("Today : " + fecha);
            Technician tec = new Technician(txttec.getText().toLowerCase(), txtemail.getText().toLowerCase());
            Equipment e = new Equipment(txtcode.getText(), String.valueOf(ctype.getValue()), txtbrand.getText(), String.valueOf(carea.getValue()), tec, String.valueOf(cstatus.getValue()));
            p.println(e.getCode() + ";" + e.getBrand() + ";" + null + ";" + null + ";" + null + ";" + null + ";" + null + ";" + null + ";" + e.getTec().getNombre() + ";" + null + ";" + null + ";" + null + ";" + null + ";" + e.getStatus() + ";" + e.getType() + "/" + e.getArea() + ";" + null + ";" + fecha);

            Alert a = new Alert(Alert.AlertType.CONFIRMATION);
            a.setTitle("Equipo registrado con éxito");
            a.setContentText("Datos registrados: " + e.toString());
            a.show();

        } catch (IOException ex) {
            Logger.getLogger(REquipment.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
