/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author pcgfd
 */
public class Main {
    private Stage stage;
    private Scene scene;
    private Label title;
    private Button nequip;
    private Button checkio;
    private Button damaged;
    private HBox box1;
    private VBox root;

    public Main(Stage s) {
        stage = s;
        inicialize();
        giveActions();
        stage.setScene(scene);
    }

    private void inicialize() {
        title = new Label("Registros y Reportes");
        nequip = new Button("Registrar equipo nuevo");
        checkio = new Button("Registrar Entrada y Salida");
        damaged = new Button("Reportar Equipo Dañado");
        box1 = new HBox(nequip, checkio);
        root = new VBox(title, box1, damaged);
        scene = new Scene(root, 500, 600);
    }

    public VBox getRoot() {
        return root;
    }

    public Scene getScene() {
        return scene;
    }
    
    
    private void giveActions() {
        nequip.setOnAction(e->open1());
        checkio.setOnAction(e->open2());
        damaged.setOnAction(e->open3());
    }
    
    
    private void open1() {
        REquipment re = new REquipment(stage);
        stage.setScene(re.getScene());
    }

    private void open2() {
        Checkio ch = new Checkio(stage);
        stage.setScene(ch.getScene());
    }

    private void open3() {
        Damaged dm = new Damaged(stage);
        stage.setScene(dm.getScene());
    }
    
    private void design(){
        
    }


}
