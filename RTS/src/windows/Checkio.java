/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author pcgfd
 */
public class Checkio {
    
    private Button entrada;
    private Button salida;
    private Button regresar;
    private Button regresarCIO;
    private Button marcare;
    private Button marcars;
    
    private Label codeE;
    private Label code;
    private Label fechasal;
    private Label horasal;
    private Label responsable;
    private Label ususalida;
    private TextField tcodeE;
    private TextField tcode;
    private TextField tfechasal;
    private TextField thorasal;
    private TextField tresponsable;
    private TextField tususalida;
    private HBox b1;
    private HBox b2;
    private HBox b3;
    private HBox b4;
    private HBox b5;
    private HBox b6;
    private HBox b7;
    private Label obse;
    
    
    private Label codeE2;
    private Label code2;
    private Label fechaen;
    private Label horaen;
    private Label usuentre;
    private TextField tcodeE2;
    private TextField tcode2;
    private TextField tfechaen;
    private TextField thoraen;
    private TextField tusuentre;
    private HBox b11;
    private HBox b22;
    private HBox b33;
    private HBox b44;
    private HBox b55;
    private HBox b66;
    private Label obse2;
    
    
    
    private VBox enroot;
    private VBox salroot;
    private VBox root;
    
    
    
    private Scene scene;
    private Stage stage;

    public Checkio(Stage s) {
        stage = s;
        iniciar();
        giveActions();
    }

    public Scene getScene() {
        return scene;
    }
    
    private void iniciar(){
        entrada = new Button("Marcar entrada");
        salida = new Button("Marcar salida");
        regresar = new Button("Regresar");
        root = new VBox(entrada, salida, regresar);
        scene = new Scene(root, 500, 600);
    }
    
    private void iniciarEntrada(){
        codeE2 = new Label("Código de Equipo: ");
        code2 = new Label("Código de transacción: ");
        fechaen = new Label("Fecha Salida: ");
        horaen = new Label("Hora de salida: ");
        usuentre = new Label("Usuario: ");
        tcode2 = new TextField();
        tcodeE2 = new TextField();
        tfechaen = new TextField();
        thoraen = new TextField();
        tusuentre = new TextField();
        regresarCIO = new Button("Regresar");
        marcare = new Button("Marcar");
        
        b11 = new HBox(code2, tcode2);
        b22 = new HBox(codeE2, tcodeE2);
        b33 = new HBox(fechaen, tfechaen);
        b44 = new HBox(horaen, thoraen);
        b55 = new HBox(usuentre, tusuentre);
        b66 = new HBox(regresarCIO, marcare);
        
        enroot = new VBox(b11,b22,b33,b44,b55);
        
    }
    
    private void iniciarSalida(){
        codeE = new Label("Código de Equipo: ");
        code = new Label("Código de transacción: ");
        fechasal = new Label("Fecha Salida: ");
        horasal = new Label("Hora de salida: ");
        responsable = new Label("Responsable: ");
        ususalida = new Label("Usuario: ");
        tcode = new TextField();
        tcodeE = new TextField();
        tfechasal = new TextField();
        thorasal = new TextField();
        tresponsable = new TextField();
        tususalida = new TextField();
        regresarCIO = new Button("Regresar");
        marcars = new Button("Marcar");
        
        b1 = new HBox(code, tcode);
        b2 = new HBox(codeE, tcodeE);
        b3 = new HBox(fechasal, tfechasal);
        b4 = new HBox(horasal, thorasal);
        b5 = new HBox(responsable, tresponsable);
        b6 = new HBox(ususalida, tususalida);
        b7 = new HBox(regresarCIO, marcars);
        
        salroot = new VBox(b1,b2,b3,b4,b5,b6);
    }
    
    private void giveActions(){
        entrada.setOnAction(e->entrada());
        salida.setOnAction(e->salida());
        regresar.setOnAction(e-> regresar());
        regresarCIO.setOnAction(e-> regresarIO());
        marcare.setOnAction(e->marcarEntrada());
        marcars.setOnAction(e->marcarSalida());
    }
    
    private void entrada(){
        iniciarEntrada();
        scene.setRoot(enroot);
    }
    
    private void salida(){
        iniciarSalida();
        scene.setRoot(salroot);
    }
    
    private void regresar() {
        Main m = new Main(stage);
        stage.setScene(m.getScene());
    }
    
    private void regresarIO() {
        Checkio m = new Checkio(stage);
        stage.setScene(m.getScene());
    }
    
    
    
    private void marcarEntrada(){
       Alert a = new Alert(Alert.AlertType.CONFIRMATION);
       a.setTitle("Entrada del equipo "+ tcodeE2.getText());
       a.setContentText("Equipo en casa.\nFecha de devolución: "+tfechaen.getText()+"\nHora: "+thoraen.getText());
       a.show();
    }
    private void marcarSalida(){
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
       a.setTitle("Salida del equipo "+ tcodeE.getText());
       a.setContentText("Equipo fuera de casa.\nFecha de salida: "+tfechasal.getText()+"\nHora: "+thorasal.getText());
       a.show();
    }
    
}
