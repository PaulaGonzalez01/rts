/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAS;

import java.util.Objects;

/**
 *
 * @author pcgfd
 */
public class Equipment {
    private String code;
    private String type;
    private String brand;
    private String area;
    private Technician tec;
    private String status;

    public Equipment(String code, String type, String brand, String area, Technician tec, String status) {
        this.code = code;
        this.type = type;
        this.brand = brand;
        this.area = area;
        this.tec = tec;
        this.status = status;
    }
    public Equipment() {
        this.code = "";
        this.type = "";
        this.brand = "";
        this.area = "";
        this.tec = null;
        this.status = "";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Technician getTec() {
        return tec;
    }

    public void setTec(Technician tec) {
        this.tec = tec;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Equipment{" + "code=" + code + ", type=" + type + ", brand=" + brand + ", area=" + area + ", tec=" + tec + ", status=" + status + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Equipment other = (Equipment) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
