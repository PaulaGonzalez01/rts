/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rts;

import javafx.application.Application;
import javafx.stage.Stage;
import windows.Main;

/**
 *
 * @author pcgfd
 */
public class RTS extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        Main m = new Main(primaryStage);
        primaryStage.setTitle("RTS RR");
        primaryStage.setScene(m.getScene());
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
